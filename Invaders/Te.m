function  Te

MainFigureHdl = [];
MainAxesHdl = [];
BirdSpriteHdl = [];
ScreenSize = get( groot, 'Screensize' ); %Get size of monitor
MainFigureSize = [600 400]; %Define size of window
MainFigureInitPos = [(ScreenSize(3)-MainFigureSize(1))/2,(ScreenSize(4)-MainFigureSize(2))/2];
CloseReq = false;
curKey = 0;
initWindow();
BirdSpriteHdl.XData = BirdSpriteHdl.XData + 300-5;
BirdSpriteHdl.YData = BirdSpriteHdl.YData + 400-30;
Dir = 1;

[y,FS] = audioread('LaserSound.wav');


for ii = 1:5
   Invaders(ii).XData =  Invaders(ii).XData + 100*ii   - 50;
end

for ii = 1:5
   Invaders(5+ii).XData =  Invaders(5+ii).XData + 100*ii;
   Invaders(5+ii).YData =  Invaders(5+ii).YData + 50;
end

pause(1)

while 1

    for j = 1:length(Invaders)
       moveInvaders(j) 
    end
    
 
       moveShots(); 
        
    
    
    
    for d=length(shots):-1:1
        if shots(d).YData <0
            deleteShots(shots(d));
            shots(d) = [];
        end
    end
    
    
    for jjj = length(shots):-1:1
        for jjjj = length(Invaders):-1:1
    delObj = dist(shots(jjj),Invaders(jjjj));
    
    if delObj == 1
        
        Invaders(jjjj) = [];
        shots(jjj) = [];
        break
    end
        end
        
    end
    

 if rand < 0.3 && length(Invaders) > 0
     invadersShots
 end
 
 if isempty(Invaders)
            waitfor(msgbox('You Won'));
            delete(MainFigureHdl);
        clear all;
       
            return
     
 end
    moveInvadersShots
    
    for d=length(shotsOfInvader):-1:1
        if shotsOfInvader(d).YData > 400
            deleteShots(shotsOfInvader(d));
            shotsOfInvader(d) = [];
        end
    end
    
    
    for jjj = length(shotsOfInvader):-1:1
        
        delObj = dist(shotsOfInvader(jjj),BirdSpriteHdl);
        
        if delObj == 1
       waitfor(msgbox('Game over'));
            delete(MainFigureHdl);
        clear all;
       
            return
        end
    end
    
   
    
    
    
move(curKey)

pause(0.03)


    if CloseReq == true
                delete(MainFigureHdl);
        clear all;
    return
    end
end




    function moveInvaders(j)
        
       Invaders(j).XData = Invaders(j).XData + Dir;
       
       if Invaders(j).XData(1) > 580
           for a = 1:length(Invaders)
       Invaders(a).YData =  Invaders(a).YData + 5;
           end
            Dir = -1;
       end
       
       if Invaders(j).XData(1) < 10
           for a = 1:length(Invaders)
               Invaders(a).YData =  Invaders(a).YData + 5;
               
           end
           Dir = 1;
       end    
    end

    function invadersShots
        
       numberOfInvaders = length(Invaders);
       selectedInvader = randi(numberOfInvaders);
       
        a = patch([0 10 10 0],[0 0 10 10],...
                'yellow',...
                'EdgeColor','none', ...
                'Visible','on', ...
                'Parent', MainAxesHdl);
            getXPosition = Invaders(selectedInvader).XData(1);
            getYPosition = Invaders(selectedInvader).YData(1);
            
            a.XData = a.XData + getXPosition;
            a.YData = a.YData + getYPosition;
        shotsOfInvader = [shotsOfInvader, a]; 
        
    end



    function moveInvadersShots
        
        for q = 1:length(shotsOfInvader)
            shotsOfInvader(q).YData = shotsOfInvader(q).YData + randi(15);
            if rand < 0.2
                if rand <=0.5
              shotsOfInvader(q).XData = shotsOfInvader(q).XData + 5;
                else
                    shotsOfInvader(q).XData = shotsOfInvader(q).XData - 5; 
                end
            end
        end
    end

    function deleteInvadersShots(InvShots)
        delete(InvShots);
    end





    function moveShots()
           for q = 1:length(shots)
        shots(q).YData = shots(q).YData - 5;
           end
    end

    function deleteShots(shots2)
        delete(shots2);
    end


    function move(dir)
        switch true
            case strcmp(dir, 'rightarrow')
                if BirdSpriteHdl.XData(1) <590
                BirdSpriteHdl.XData = BirdSpriteHdl.XData + 10;
                end
            case strcmp(dir, 'leftarrow')
                if BirdSpriteHdl.XData(1) >0
                BirdSpriteHdl.XData = BirdSpriteHdl.XData - 10;
                end
        end
    end

    function flag = dist(shots2,invaders2)
        centerShots2X = shots2.XData(1);
        centerInvaders2X = invaders2.XData(1);
        centerShots2Y = shots2.YData(1);
        centerInvaders2Y = invaders2.YData(1);
        distanceX = centerShots2X - centerInvaders2X;
         distanceY = centerShots2Y - centerInvaders2Y;
         if abs(distanceY) < 30 && abs(distanceX) <10
           flag = 1;
          delete(invaders2)
          delete(shots2)
         else
             flag = 0;
         end
    end

    function initWindow()
        %Get screen size
        
        
        % initWindow - initialize the main window, axes and image objects
        MainFigureHdl = figure('Name', ['Flappy Bird ' 1], ...
            'NumberTitle' ,'off', ...
            'Position', [MainFigureSize,MainFigureInitPos], ...
            'Units', 'pixels', ...
            'MenuBar', 'figure', ...
            'Renderer', 'OpenGL',...
            'Color',[0 0 0], ...
            'KeyPressFcn', @stl_KeyPressFcn, ...
            'WindowKeyReleaseFcn', @stl_KeyUp,...
            'CloseRequestFcn', @stl_CloseReqFcn);
        
        
        MainAxesHdl = axes('Parent', MainFigureHdl, ...
            'Units', 'pixels',...
            'color', [1 1 1], ...
             'Position', [0,0,MainFigureInitPos], ...
            'XLim', [0 600]-0.5, ...
            'YLim', [0 400]-0.5, ...
            'YDir', 'reverse', ...
            'NextPlot', 'add', ...
            'Visible', 'on', ...
            'XTick',[], ...
            'YTick',[]);
        
        %                MainCanvasHdl = image([0 144-1], [0 200-1], [],...
        %             'Parent', MainAxesHdl,...
        %             'Visible', 'on');
        
        
        BirdSpriteHdl = patch([0 10 10 0],[0 0 30 30],...
            'red',...
            'EdgeColor','none', ...
            'Visible','on', ...
            'Parent', MainAxesHdl);        
        
  
        for i = 1:11
            Invaders(i) = patch([0 25 25 0],[0 0 25 25],...
                'green',...
                'EdgeColor','none', ...
                'Visible','on', ...
                'Parent', MainAxesHdl);
        end
        
        for i = 1:2
        shots(i) = patch([0 25 25 0],[0 0 25 25],...
                'blue',...
                'EdgeColor','none', ...
                'Visible','on', ...
                'Parent', MainAxesHdl);
        end
        
        for i =1:2
            shotsOfInvader(i) = patch([0 25 25 0],[0 0 25 25],...
                'yellow',...
                'EdgeColor','none', ...
                'Visible','on', ...
                'Parent', MainAxesHdl);
        end
    end


    function stl_CloseReqFcn(hObject, eventdata, handles)
        CloseReq = true;
    end


    function stl_KeyPressFcn(hObject, eventdata, handles)
        
          curKey2 = get(hObject, 'CurrentKey');
        
        
        
        
        if strcmp(curKey2, 'space') == 1 && length(shots)<6
            
            a = patch([0 10 10 0],[0 0 10 10],...
                'blue',...
                'EdgeColor','none', ...
                'Visible','on', ...
                'Parent', MainAxesHdl);
            getXPosition = BirdSpriteHdl.XData(1);
            getYPosition = BirdSpriteHdl.YData(1);
            
            a.XData = a.XData + getXPosition;
            a.YData = a.YData + getYPosition;
        shots = [shots, a]; 
        sound(y,FS)
        else
            curKey = get(hObject, 'CurrentKey');
        end
        
    end

    function stl_KeyUp(hObject, eventdata, handles)
        curKey3 = get(hObject, 'CurrentKey');
        if strcmp(curKey3, 'space') ~=1
       curKey = 0;
        end
    end

end