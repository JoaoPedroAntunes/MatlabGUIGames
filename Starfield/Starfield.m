function Starfield
global CloseReq
%Who can change the CanvasWidth, CanvasHeight, size of the star and number
%of stars.
CanvasWidth = 400;
CanvasHeight = 400;
SizeOfStars = 5;
NumberOfStars = 100;


CenterCanvasWidth = CanvasWidth/2;
CenterCanvasHeight = CanvasHeight/2;


CloseReq = 0;
[Canvas, MainCanvas] = CreateCanvas(CanvasWidth,CanvasHeight);

sx = zeros(NumberOfStars);
sy = zeros(NumberOfStars);

%Generate Stars and Velocity
for j = 1:NumberOfStars
    Stars(j) = circulo(randi([-CenterCanvasWidth CenterCanvasWidth]),randi([-CenterCanvasHeight CenterCanvasHeight]),randi(SizeOfStars),'white');
    StarsVelocity(j) = randi(100);
end

%Allow time to render
pause(1)
time = 0;
Processed = 0;

while 1
    tic
    %0.03s => 30 frames per second
    %While 0.03s don't pass just be in this loop
    while time < 0.03
        
        DrawStars
        time = toc;
    end
    %If time of 0.03 as passed render image
    drawnow;
    Processed = 0;
    time = 0;
    
    if CloseReq ==1
        delete(Canvas)
        clear all
        return
    end
end



    function DrawStars
        %This ensures that while inside the while loop this function only
        %runs once every 0.03s
        if Processed == 0
            for i = 1:NumberOfStars
                %In each new position increase velocity
                StarsVelocity(i) = StarsVelocity(i) - 1;
                %If velocity is smaller then 1, the star is of the image. 
                %Draw it again in a new position
                if StarsVelocity(i) < 1
                    StarsVelocity(i) = randi(100);
                    Stars(i).Position(1) =  randi([-CenterCanvasWidth CenterCanvasWidth]);
                    Stars(i).Position(2) =  randi([-CenterCanvasHeight CenterCanvasHeight]);
                    r =randi(SizeOfStars);
                    Stars(i).Position(3:4) = [r r];
                end
                %Calculate velocity based on the position of star relative
                %to the width of the image
                sx(i) = 0 + (CenterCanvasWidth -(0))*((Stars(i).Position(1)/ StarsVelocity(i)-0)/(CenterCanvasWidth-0));
                sy(i) = 0 + (CenterCanvasWidth -(0))*((Stars(i).Position(2)/ StarsVelocity(i)-0)/(CenterCanvasWidth-0));
                %Update Position o fthe star
                Stars(i).Position(1) = Stars(i).Position(1)+ sx(i);
                Stars(i).Position(2) = Stars(i).Position(2)+  sy(i);
                %Increase size of star
                Stars(i).Position(3:4) = Stars(i).Position(3:4) + 0.05;
                
                Processed = 1;
            end
        end
    end
end